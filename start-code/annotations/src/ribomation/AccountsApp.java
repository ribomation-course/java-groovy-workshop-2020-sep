package ribomation;

import ribomation.api.DDL;

public class AccountsApp {
    public static void main(String[] args) throws Exception {
        AccountsApp app = new AccountsApp();
        app.generateTable("ribomation.domain.Account");
        app.generateTable("ribomation.domain.Invoice");
    }

    void generateTable(String clsName) throws Exception {
        System.out.printf("--- DDL SQL ---\n%s%n", DDL.instance.generateTable(clsName));
    }
}
