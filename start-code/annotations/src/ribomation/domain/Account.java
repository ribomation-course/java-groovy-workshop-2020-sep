package ribomation.domain;

import ribomation.api.annotations.*;
import java.util.Objects;

@Table
public class Account {
    @Column("varchar(12)")
    @PrimaryKey
    private String accNo;

    @Column("varchar(32)")
    private String customer;

    @Column("decimal(12,2)")
    private double balance;

    @Column("float")
    private float rate;

    @Column("tinybool")
    private boolean limited;

    public Account() { }

    public Account(String csv) {
        //todo: implement this constructor
    }

    @Override
    public String toString() {
        return "Account{" +
                "accNo='" + accNo + '\'' +
                ", customer='" + customer + '\'' +
                ", balance=" + balance +
                ", rate=" + rate +
                ", limited=" + limited +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Double.compare(account.getBalance(), getBalance()) == 0 &&
                Float.compare(account.rate, rate) == 0 &&
                limited == account.limited &&
                Objects.equals(getAccNo(), account.getAccNo()) &&
                Objects.equals(getCustomer(), account.getCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccNo(), getCustomer(), getBalance(), rate, limited);
    }

    public String getAccNo() {
        return accNo;
    }

    public String getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public float getRate() {
        return rate;
    }

    public boolean isLimited() {
        return limited;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public void setLimited(boolean limited) {
        this.limited = limited;
    }
}
