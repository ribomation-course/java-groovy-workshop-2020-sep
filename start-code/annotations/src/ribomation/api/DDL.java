package ribomation.api;

import ribomation.api.annotations.Column;
import ribomation.api.annotations.Table;
import ribomation.api.helpers.ColumnDef;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DDL {
    public static final DDL instance = new DDL();

    private DDL() {
    }

    public String generateTable(String cls) throws ClassNotFoundException {
        return generateTable(Class.forName(cls));
    }

    public String generateTable(Class cls) {
        Annotation table = cls.getAnnotation(Table.class);
        if (table == null) throw new RuntimeException("Missing @Table");

        return String.format("CREATE TABLE %s (%n%s%n);",
                cls.getSimpleName().toLowerCase() + "s",
                format(extractColumns(cls))
        );
    }

    private List<ColumnDef> extractColumns(Class cls) {
        return Stream.of(cls.getDeclaredFields())
                .filter((f) -> f.getAnnotation(Column.class) != null)
                .map(ColumnDef::new)
                .collect(Collectors.toList());
    }

    private String format(List<ColumnDef> columns) {
        final var maxWidth = columns.stream()
                .map(ColumnDef::getName)
                .mapToInt(String::length)
                .max()
                .orElse(0);

        return columns.stream()
                .map(c -> String.format("  %-" + maxWidth + "s  %s", c.getName(), c.getType()))
                .collect(Collectors.joining("," + System.lineSeparator()));
    }

}
