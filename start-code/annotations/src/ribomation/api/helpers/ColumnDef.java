package ribomation.api.helpers;

import ribomation.api.annotations.Column;
import ribomation.api.annotations.PrimaryKey;

import java.lang.reflect.Field;

public class ColumnDef {
    String name;
    String type;
    boolean pk;

    public ColumnDef(Field f) {
        name = f.getName().toLowerCase();
        type = f.getAnnotation(Column.class).value().toUpperCase();

        PrimaryKey primaryKey = f.getAnnotation(PrimaryKey.class);
        if ((primaryKey != null)) {
            pk = true;
        }
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return String.format("%s%s", type, pk ? " PRIMARY KEY" : "");
    }
}
