package ribomation;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.domain.PersonDAO;

public class App {
    public static void main(String[] args) { new App().run(); }

    public void run() {
        var ctx = new ClassPathXmlApplicationContext("/app.xml");
        var db = ctx.getBean("dao", PersonDAO.class);

        System.out.println("--- findAll ---");
        db.findAll().forEach(System.out::println);

        System.out.println("--- findById ---");
        System.out.printf("id=1: %s%n", db.findById(1).orElseThrow());
        System.out.printf("id=111: %s%n", db.findById(111).orElse(null));

        System.out.println("--- findAllByCountry ---");
        db.findAllByCountry("Sweden").forEach(System.out::println);
    }
}

