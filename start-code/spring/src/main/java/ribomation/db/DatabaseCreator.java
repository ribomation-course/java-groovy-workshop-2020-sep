package ribomation.db;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DatabaseCreator {
    private final String TABLE = "persons";
    private final JdbcTemplate db;

    public DatabaseCreator(DataSource ds) {
        this.db = new JdbcTemplate(ds);
    }

    public void setup() {
        dropTable();
        createTable();
        populate();
    }

    public void dropTable() {
        db.update("DROP TABLE IF EXISTS " + TABLE);
    }

    public void createTable() {
        final var sql = "CREATE TABLE IF NOT EXISTS " + TABLE + " (" +
                "  id       INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                "  name     VARCHAR(32) NOT NULL," +
                "  age      INT NOT NULL," +
                "  country  VARCHAR(32) NOT NULL" +
                ");";
        db.update(sql);
    }

    public void populate() {
        final var sql = "INSERT INTO " + TABLE + " (name,age,country) VALUES (?,?,?)";
        var is = getClass().getResourceAsStream("/data.csv");
        var reader = new BufferedReader(new InputStreamReader(is));
        reader.lines().skip(1)
                .map(csv -> csv.split(","))
                .forEach(fields -> {
                    var name = fields[0];
                    var age = Integer.parseInt(fields[1]);
                    var country = fields[2];
                    db.update(sql, name, age, country);
                });
    }
}

