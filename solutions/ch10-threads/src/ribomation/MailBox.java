package ribomation;

public class MailBox<T> {
    private T payload;
    private boolean busy = false;

    public synchronized void put(T msg) {
        try {
            while (busy) wait();
            busy = true;
            payload = msg;
        } catch (InterruptedException ignore) {
        } finally {
            notifyAll();
        }
    }

    public synchronized T get() {
        try {
            while (!busy) wait();
            busy = false;
            return payload;
        } catch (InterruptedException ignore) {
            return null;
        } finally {
            notifyAll();
        }
    }

}
