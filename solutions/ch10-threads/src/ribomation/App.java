package ribomation;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run(1000);
    }

    void run(final int N) throws InterruptedException {
        final var mb = new MailBox<Integer>();

        var consumer = new Thread(() -> {
            for (var msg = mb.get(); msg != -1; msg = mb.get()) {
                System.out.printf("[consumer] %d%n", msg);
            }
            System.out.printf("[consumer] done%n");
        });

        var producer = new Thread(() -> {
            for (var k = 1; k <= N; ++k) mb.put(k);
            mb.put(-1);
            System.out.printf("[producer] sent %d messages%n", N);
        });

        consumer.start();
        producer.start();
        producer.join();
        consumer.join();
    }

}
