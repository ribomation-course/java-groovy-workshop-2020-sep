package ribomation.fmt;


import ribomation.result.Result;

import java.io.Writer;
import java.util.List;

/**
 * Transforms a result object into a formatted output
 */
public interface Formatter {
    void format(List<Result> result, Writer out);
}
