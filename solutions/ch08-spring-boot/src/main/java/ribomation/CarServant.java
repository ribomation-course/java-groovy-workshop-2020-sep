package ribomation;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ribomation.domain.Car;
import ribomation.domain.CarRepository;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarServant {
    private final CarRepository repo;

    public CarServant(@Qualifier("setup") CarRepository repo) {
        this.repo = repo;
    }

    @GetMapping
    public List<Car> all() {
        return repo.findAll();
    }

    @GetMapping("{id}")
    public Car one(@PathVariable Integer id) {
        var obj = repo.findById(id);
        if (obj.isPresent()) {
            return obj.get();
        } else {
            throw new RuntimeException("not found: id=" + id);
        }
    }

}
