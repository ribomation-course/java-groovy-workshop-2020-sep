package ribomation.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class Car {
    private final int id;
    private static int nextId = 1;
    private final String vendor;
    private final String model;
    private final int year;

    public Car(int id, String vendor, String model, int year) {
        this.id = id;
        this.vendor = vendor;
        this.model = model;
        this.year = year;
    }

    public static Car fromCSV(String csv) {
        return fromCSV(csv, ',');
    }
    public static Car fromCSV(String csv, char delim) {
        var fields = csv.split(String.valueOf(delim));
        var vendor = fields[0];
        var model = fields[1];
        var year = Integer.parseInt(fields[2]);
        return new Car(nextId++, vendor, model, year);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("vendor='" + vendor + "'")
                .add("model='" + model + "'")
                .add("year=" + year)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id &&
                year == car.year &&
                vendor.equals(car.vendor) &&
                model.equals(car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vendor, model, year);
    }

    public int getId() {
        return id;
    }

    public String getVendor() {
        return vendor;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }
}
