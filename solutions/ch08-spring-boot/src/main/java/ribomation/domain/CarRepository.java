package ribomation.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
public class CarRepository {
    private List<Car> repo = new ArrayList<>();

    @Bean
    public CarRepository setup() {
        repo = loadCSV("/data.csv");
        return this;
    }

    public List<Car> loadCSV(String classpathResource) {
        var is = getClass().getResourceAsStream(classpathResource);
        if (is == null) {
            throw new RuntimeException("cannot open " + classpathResource);
        }
        return new BufferedReader(new InputStreamReader(is))
                .lines()
                .skip(1)
                .map(Car::fromCSV)
                .collect(Collectors.toList());
    }

    public List<Car> findAll() {
        return repo;
    }

    public Optional<Car> findById(int id) {
        return repo.stream().filter(obj -> obj.getId() == id).findFirst();
    }

}
