window.onload = () => {
    const baseUrl = 'http://localhost:5000'
    const result = document.querySelector("#result")

    document
        .querySelector("#one")
        .addEventListener("click", ev => {
            ev.preventDefault()
            result.innerHTML = '';

            (async () => {
                const oneId=document.querySelector('#oneId')
                const id = +(oneId.value || 1)
                const response = await fetch(`${baseUrl}/persons/${id}`)
                if (response.status === 200) {
                    const payload = await response.json()
                    console.info('payload: %o', payload)
                    result.innerHTML = JSON.stringify(payload, null, 2)
                } else {
                    result.innerHTML = 'NOT FOUND'
                }
            })();
        })

    document
        .querySelector("#all")
        .addEventListener('click', ev => {
            ev.preventDefault()
            result.innerHTML = '';

            (async () => {
                const response = await fetch(`${baseUrl}/persons`)
                const payload = await response.json()
                console.info('payload: %o', payload)
                result.innerHTML = JSON.stringify(payload, null, 2)
            })();
        })

    document.querySelector('#version')
        .addEventListener('click', ev => {
            ev.preventDefault()
            result.innerHTML = '';

            (async () => {
                const response = await fetch(`${baseUrl}/version`)
                const payload = await response.text()
                console.info('payload: %o', payload)
                result.innerHTML = payload
            })();
        })

}
