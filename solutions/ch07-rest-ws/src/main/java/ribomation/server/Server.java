package ribomation.server;

import com.google.gson.Gson;
import ribomation.domain.PersonRepo;

import java.util.Optional;

import static spark.Spark.port;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.staticFiles;

public class Server {
    public static void main(String[] args) {
        var app = new Server();
        app.setup();
        app.routes();
    }

    PersonRepo repo = new PersonRepo();
    Gson json = new Gson();

    void setup() {
        repo.populate(10);
        port(5000);
        staticFiles.location("/public");
    }

    void routes() {
        get("/version", (req, res) -> {
            return "Silly Server, version 0.42";
        });

        get("/persons", (req, res) -> repo.all(), json::toJson);

        get("/persons/:id", (req, res) -> {
            var id = Integer.parseInt(req.params("id"));
            var maybe = repo.findById(id);
            if (maybe.isEmpty()) halt(404);
            return maybe.get();
        }, json::toJson);

    }

}
