package ribomation.client;

import com.goebl.david.Webb;
import ribomation.domain.Person;

public class Client {
    public static void main(String[] args) throws Exception {
        var app = new Client();
        app.run();
    }

    void run() throws Exception {
        var json = Webb.create()
                .get("http://localhost:5000/persons/1")
                .ensureSuccess()
                .asJsonObject()
                .getBody();

        System.out.printf("json  : %s%n", json);
        var p = new Person(json.getInt("id"), json.getString("name"), json.getInt("age"));
        System.out.printf("person: %s%n", p);
    }
}
