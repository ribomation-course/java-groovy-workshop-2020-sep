package ribomation.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class PersonRepo {
    private final Random r = new Random();
    private final List<Person> db = new ArrayList<>();

    public void populate(int n) {
        for (var k = 0; k < n; ++k) db.add(createPerson(k + 1));
    }

    public void clear() {
        db.clear();
    }

    public int size() {
        return db.size();
    }
    public List<Person> all() {
        return db;
    }

    public Optional<Person> findById(int id) {
        return db.stream().filter(p -> p.getId() == id).findFirst();
    }

    public Person createPerson(int id) {
        var name = names[r.nextInt(names.length)];
        var age = 20 + r.nextInt(65);
        return new Person(id, name, age);
    }

    private static final String[] names = {
            "Anna", "Berit", "Carin", "Doris", "Eva", "Frida",
            "Adam", "Bertil", "Carl", "Daniel", "Erik", "Filip"
    };
}
