public class RepeatApp {
    interface RepeatHandler {
        void doit(int k);
    }

   void repeat(int n, RepeatHandler expr) {
        for (int k=1; k<=n; ++k) {
            expr.doit(k);
        }
    } 

    public static void main(String[] args) {
        System.out.println("using repeat:");

        var app = new RepeatApp();
        app.repeat(5, x -> System.out.printf("[%d] Java is Cool!%n", x));
        
        //Groovy: 5.times { println 'Groovy is even cooler' }
    }
}
