package ribomation;
public class ReflectionApp {
    public static void main(String[] args) throws Exception {
        final String clsName = "ribomation.Account";
        System.out.println(clsName);

        var cls = Class.forName(clsName);
        System.out.println(cls);

        var cons = cls.getDeclaredConstructor(String.class);
        System.out.println(cons);

        var obj = cons.newInstance("SEB1234");
        System.out.println(obj);

        var update = cls.getDeclaredMethod("update", int.class);
        System.out.println(update);

        update.invoke(obj, 100);
        System.out.println(obj);

        var bal = cls.getDeclaredField("balance");
        System.out.println(bal);

        bal.setAccessible(true);
        bal.set(obj, 42);
        System.out.println(obj);
    }
}
