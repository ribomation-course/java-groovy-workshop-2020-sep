package ribomation ;
public class Account {
        private String accno;
        private int balance;
        public Account(String accno) {this.accno = accno;}
        public String getAccno() {return accno;}
        public int getBalance() {return balance;}
        public void update(int amount) {balance += amount;}
        public String toString() {
            return String.format("Account[%s, %d kr]", accno, balance);
        }
    }
