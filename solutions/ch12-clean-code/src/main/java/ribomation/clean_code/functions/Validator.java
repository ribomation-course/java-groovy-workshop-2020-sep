package ribomation.clean_code.functions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Validator {
    private final Calendar date;

    public Validator(Calendar date) {
        this.date = date;
    }

    String normalize(String pnr) {
        if (pnr.length() == 12) {
            return pnr;
        }
        if (pnr.length() == 13) {
            return pnr.replaceFirst("[+-]", "");
        }
        if (pnr.contains("+")) {
            return "19" + pnr.replaceFirst("[+]", "");
        }

        pnr = pnr.replaceFirst("[-]", "");
        int birthYear = Integer.parseInt(pnr.substring(0, 2));
        int currentYear = date.get(Calendar.YEAR);

        if (currentYear < (2000 + birthYear)) {
            return "19" + pnr;
        }

        if (currentYear == (2000 + birthYear) && isCentenarian(pnr)) {
            return "19" + pnr;
        } else {
            return "20" + pnr;
        }
    }

    private boolean isCentenarian(String pnr) {
        String todayStr = new SimpleDateFormat("yyyyMMdd").format(date.getTime());
        String birthStr = "20" + pnr.substring(0, 6);
        return birthStr.compareTo(todayStr) <= 0;
    }

}

