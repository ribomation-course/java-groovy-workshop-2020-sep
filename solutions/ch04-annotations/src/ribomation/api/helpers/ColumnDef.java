package ribomation.api.helpers;

import ribomation.api.annotations.Column;
import ribomation.api.annotations.Default;
import ribomation.api.annotations.NotNull;
import ribomation.api.annotations.PrimaryKey;

import java.lang.reflect.Field;

public class ColumnDef {
    String name;
    String type;
    boolean pk;
    boolean autoIncrement;
    boolean notNull;
    String defval;

    public ColumnDef(Field f) {
        name = f.getName().toLowerCase();
        type = f.getAnnotation(Column.class).value().toUpperCase();
        notNull = (f.getAnnotation(NotNull.class) != null);
        defval = (f.getAnnotation(Default.class) != null) ? f.getAnnotation(Default.class).value() : null;

        PrimaryKey primaryKey = f.getAnnotation(PrimaryKey.class);
        if ((primaryKey != null)) {
            pk = true;
            autoIncrement = primaryKey.autoIncrement();
        }
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return String.format("%s%s%s%s%s",
                type,
                pk ? " PRIMARY KEY" : "",
                autoIncrement ? " AUTO-INCREMENT" : "",
                notNull ? " NOT NULL" : "",
                defval != null ? " DEFAULT " + defval : "");
    }
}
