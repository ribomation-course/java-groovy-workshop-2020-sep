package ribomation.db;

import ribomation.domain.Person;
import ribomation.domain.PersonDAO;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class DatabasePersonDAO implements PersonDAO {
    private final String TABLE = "persons";
    private final JdbcTemplate db;
    private final PersonMapper mapper = new PersonMapper();

    public DatabasePersonDAO(DataSource ds) {
        this.db = new JdbcTemplate(ds);
    }

    @Override
    public List<Person> findAll() {
        final var sql = "select id,name,age,country from " + TABLE;
        return db.query(sql, mapper);
    }

    @Override
    public Optional<Person> findById(int id) {
        final var sql = "select id,name,age,country from " + TABLE + " WHERE id = ?";
        try {
            return Optional.ofNullable(db.queryForObject(sql, mapper, id));
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> findAllByCountry(String country) {
        final var sql = "select id,name,age,country from " + TABLE + " WHERE country = ?";
        return db.query(sql, mapper, country);
    }

    @Override
    public List<Person> findAllByAgeRange(int lb, int ub) {
        final var sql = "select id,name,age,country from " + TABLE + " WHERE age BETWEEN ? AND ?";
        return db.query(sql, mapper, lb, ub);
    }
}

