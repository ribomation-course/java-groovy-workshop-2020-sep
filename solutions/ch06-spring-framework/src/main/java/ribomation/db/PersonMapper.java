package ribomation.db;

import ribomation.domain.Person;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        var id = rs.getInt("id");
        var name = rs.getString("name");
        var age = rs.getInt("age");
        var country = rs.getString("country");

        return new Person(id, name, age, country);
    }
}

