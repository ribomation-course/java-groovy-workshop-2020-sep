package ribomation;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.domain.PersonDAO;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    public void run() {
        var ctx = new ClassPathXmlApplicationContext("/app.xml");
        var db = ctx.getBean("dao", PersonDAO.class);

        var size=0;
        {
            System.out.println("--- findAll ---");
            size = db.findAll().size();
            System.out.printf("loaded %d records%n", size);
        }

        {
            System.out.println("--- findById ---");
            System.out.printf("id=1: %s%n", db.findById(1).orElseThrow());
            var idNotFound = size * 3;
            System.out.printf("id=%d: %s%n", idNotFound, db.findById(idNotFound).orElse(null));
        }

        {
            var country = "Sweden";
            System.out.printf("--- findAllByCountry: %s ---%n", country);
            size = db.findAllByCountry("Sweden").size();
            System.out.printf("found %d records%n", size);
        }

        {
            var lb = 30;
            var ub = 40;
            System.out.printf("--- findAllByAgeRange: [%d, %d] ---%n", lb,ub);
            size = db.findAllByAgeRange(lb, ub).size();
            System.out.printf("found %d records%n", size);
        }
    }
}

