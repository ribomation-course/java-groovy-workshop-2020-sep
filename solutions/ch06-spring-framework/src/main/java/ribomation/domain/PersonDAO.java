package ribomation.domain;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {
    List<Person> findAll();
    Optional<Person> findById(int id);
    List<Person> findAllByCountry(String country);
    List<Person> findAllByAgeRange(int lb, int ub);
}

