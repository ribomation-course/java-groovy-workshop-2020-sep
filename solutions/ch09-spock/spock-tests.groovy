@Grapes(
    @Grab(group='org.spockframework', module='spock-core', version='2.0-M3-groovy-3.0', scope='test')
)
import spock.lang.*
public class tester extends Specification {
    def 'making text to UPPERCASE'() {
        given: 'a silly text'
        def txt = 'FooBar Strikes Again'
        when: 'we invoke toUpperCase'
        def result = txt.toUpperCase()
        then: 'all chars should now be captials'
        result == 'FOOBAR STRIKES AGAIN'
    }
    
    def 'reversing a string'() {
        given: 'a silly text'
        def txt = 'FooBar'
        
        when: 'we invoke reverse'
        def result = txt.reverse()
        
        then: 'all chars should be backwards'
        result == 'raBooF'
    }
    
    def 'appending to a list'() {
        given: 'a lousy list'
        def lst = [1,2,3]
        def sizeOrig = lst.size()
        
        when: 'appending the list'
        lst << 10 << 20
        
        then: 'we should have the combined list and the size should be +2'
        lst == [1,2,3,10,20]
        lst.size() == sizeOrig + 2
    }
    
    def 'using a stack'() {
        given: 'a boring stack'
        def stk = new java.util.Stack()
        
        when: 
        stk.push('one')       
        stk.push('two')       
        then:
        stk.size() == 2
        
        when:
        stk.pop()
        then:
        stk.size() == 1
        
        when:
        stk.pop()
        then:
        stk.isEmpty()
    }
    
    def 'checking Gauss'() {
        given: 'a number'
        def N = 10
        
        when: 'sum from 1 to N'
        def result = (1..N).sum()
        
        then: 'it should match Gauss summation formula'
        result == N * (N + 1) / 2
    }
}
