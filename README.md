# Java Groovy Workshop
### 2-3 September 2020, Gothenburg

# Links
* [Installation Instructions](./installation-instructions.md)
* [Course Details](./java-groovy-workshop.pdf)

Usage of this GIT Repo
====

Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <https url to repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a git pull operation

    cd ~/cxx-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

