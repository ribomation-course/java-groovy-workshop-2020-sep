package ribomation.java_advanced.atm;

public class Account {
    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    public void updateBalance(int amount) {
        int b = getBalance();   //READ
        b += amount;            //MODIFY
        this.balance = b;       //WRITE
    }

    @Override
    public String toString() {
        return "Account{balance=" + balance + '}';
    }
}
