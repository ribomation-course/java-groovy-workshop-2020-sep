package ribomation.java_advanced.swift;

public class SafeSwiftApp extends UnsafeSwiftApp {
    public static void main(String[] args) {
        new SafeSwiftApp().run(args);
    }

    @Override
    protected MoneyBox createMoneyBox() {
        return new MoneyBox() {
            private boolean empty = true;

            @Override
            public synchronized void send(int amount) {
                try {
                    while (!empty) wait();
                    empty = false;
                    super.send(amount);
                } catch (InterruptedException ignore) {
                } finally {
                    notifyAll();
                }
            }

            @Override
            public synchronized int recv() {
                try {
                    while (empty) wait();
                    empty = true;
                    return super.recv();
                } catch (InterruptedException ignore) {
                    return STOP;
                } finally {
                    notifyAll();
                }
            }
        };
    }
}
